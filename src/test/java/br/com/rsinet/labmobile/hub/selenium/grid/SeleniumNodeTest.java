// Licensed to the Software Freedom Conservancy (SFC) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The SFC licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package br.com.rsinet.labmobile.hub.selenium.grid;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;

import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;

public class SeleniumNodeTest {

	private static Path nodeConfigFile;
	private SeleniumNode node = SeleniumNode.newInstance(nodeConfigFile);
	@Rule
	public ExpectedSystemExit exit = ExpectedSystemExit.none();

	@BeforeClass
	public static void setUpClass() throws URISyntaxException {
		URI hubConfigLocation = ClassLoader.getSystemResource("firefoxNodeConfig.json").toURI();
		nodeConfigFile = Paths.get(hubConfigLocation);
	}

	@After
	public void tearDown() {
		if (node.isRunning()) {
			node.shutdown();
		}
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowExceptionCauseConfigFileIsNull() {
		SeleniumNode.newInstance(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowExceptionCauseConfigFileIsMissing() {
		SeleniumNode.newInstance(Paths.get("any.json"));
	}

	@Test
	public void shouldStartNode() {
		node.start();
		Response response = ClientBuilder.newClient().target(node.getUrl()).path("grid/api/hub/").request().get();
		Assert.assertEquals(200, response.getStatus());
	}

	@Test(expected = IllegalStateException.class)
	public void shouldThrowExceptionCauseAlreadyStartedUp() {
		node.start();
		node.start();
	}

	@Test(expected = ProcessingException.class)
	public void shouldStop() {
		node.start();
		String url = node.getUrl();
		node.shutdown();

		ClientBuilder.newClient().target(url).path("grid/api/hub/").request().get();
	}

	@Test(expected = IllegalStateException.class)
	public void shouldThrowExceptionCauseItIsAlreadyStopped() {
		node.shutdown();
	}

	@Test
	public void shouldReturnFalseCauseItIsStopped() {
		Assert.assertFalse(node.isRunning());
	}

	@Test
	public void shouldHasDifferentPorts() {
		node.start();
		String url = node.getUrl();
		SeleniumNode node2 = SeleniumNode.newInstance(nodeConfigFile);
		node2.start();
		String url2 = node2.getUrl();

		Assert.assertNotEquals(url, url2);
		node2.shutdown();
	}

}