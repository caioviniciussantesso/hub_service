package br.com.rsinet.labmobile.hub.selenium.grid;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;

import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 * 
 * @author caiosantesso
 *
 */
public class SeleniumHubTest {

	private static Path hubConfig;
	private static final String URI = "http://localhost:4444";
	private SeleniumHub hub = SeleniumHub.getInstance();

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@BeforeClass
	public static void setUpClass() throws URISyntaxException {
		URI hubConfigLocation = ClassLoader.getSystemResource("hubConfig.json").toURI();
		hubConfig = Paths.get(hubConfigLocation);

	}

	@After
	public void tearDown() {
		if (hub.isRunning()) {
			hub.shutdown();
		}
	}

	@Test
	public void shouldStartSeleniumHub() {
		hub.start(hubConfig);

		Response response = ClientBuilder.newClient().target(URI).path("grid/api/hub/").request().get();
		Assert.assertEquals(200, response.getStatus());
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowExceptionBecauseConfigFileDoesNotExist() {
		hub.start(Paths.get("nenhum_arquivo"));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowExceptionBecauseConfigFileIsNull() {
		hub.start(null);
	}
	
	@Test(expected = ProcessingException.class)
	public void shouldShutdownSeleniumHub() {
		hub.start(hubConfig);
		hub.shutdown();

		ClientBuilder.newClient().target(URI).path("grid/api/hub/").request().get();
	}

	@Test(expected = IllegalStateException.class)
	public void shouldThrowExceptionCauseAlreadyStopped() {
		hub.shutdown();
	}

	@Test(expected = IllegalStateException.class)
	public void shouldThrowExceptionCauseAlreadyStartedUp() {
		hub.start(hubConfig);
		hub.start(hubConfig);
	}

	@Test
	public void shouldReturnFalseCauseIsStopped() {
		Assert.assertFalse(hub.isRunning());
	}
	
}