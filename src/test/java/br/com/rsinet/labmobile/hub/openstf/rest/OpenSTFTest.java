package br.com.rsinet.labmobile.hub.openstf.rest;

import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import br.com.rsinet.labmobile.hub.openstf.rest.OpenSTF;
import br.com.rsinet.labmobile.hub.openstf.rest.json.Device;
import br.com.rsinet.labmobile.hub.openstf.rest.json.User;

/**O Opent STF deve estar operacional.
 * <p>
 * Após cada teste unitário, deve ser garantido que os celulares estão
 * destravados (sem usuários atrelados).
 * <p>
 * As constantes <strong>NAME</strong>, <strong>SERIAL</strong>,
 * <strong>TOKEN</strong> e <strong>URI</strong> devem ser definidas antes de
 * cada execução desta classe considerando o usuário OpenSTF que será utilizado
 * para tal execução.
 * 
 * @author caiosantesso
 *
 */
@Ignore
public class OpenSTFTest {

	private static final String NAME = "Santesso";
	private static final String SERIAL = "0422443110";
	private static final String TOKEN = "82dcfe0f289e42d385388cb87473ea64561dc96aa86c4258aaa833f413933cc6";
	private static final String URI = "http://localhost:7100/";
	private static final OpenSTF STF = new OpenSTF.Builder().uri(URI).token(TOKEN).build();
	private static final OpenSTF STF_INVALID_TOKEN = new OpenSTF.Builder().uri(URI).token("!").build();
	private static final int LOCKED_DEVICES = 1;
	/**
	 * Definir de acordo com a quantidade de dispositivos que são exibidos no
	 * OpenSTF.
	 */
	private static final int CONNECTED_DEVICES = 1;

	@Test
	public void shouldLockDevice() {
		STF.unlockDevice(SERIAL);

		boolean lockDevice = STF.lockDevice(SERIAL);
		Assert.assertTrue(lockDevice);

		STF.unlockDevice(SERIAL);
	}

	@Test
	public void shouldNotLockDeviceCauseInvalidToken() {
		boolean lockDevice = STF_INVALID_TOKEN.lockDevice(SERIAL);
		Assert.assertFalse(lockDevice);
	}

	@Test
	public void shouldNotLockDeviceCauseAlreadyLocked() {
		STF.lockDevice(SERIAL);
		boolean lockDevice = STF.lockDevice(SERIAL);
		Assert.assertFalse(lockDevice);
	}

	@Test
	public void shouldUnlockDevice() {
		STF.lockDevice(SERIAL);

		boolean unlockDevice = STF.unlockDevice(SERIAL);
		Assert.assertTrue(unlockDevice);
	}

	@Test
	public void shouldNotUnlockDeviceCauseInvalidToken() {
		STF.lockDevice(SERIAL);

		boolean unlockDevice = STF_INVALID_TOKEN.unlockDevice(SERIAL);
		Assert.assertFalse(unlockDevice);

		STF.unlockDevice(SERIAL);
	}

	@Test
	public void shouldNotUnlockDeviceCauseAlreadyUnlocked() {
		STF.unlockDevice(SERIAL);

		boolean unlockDevice = STF_INVALID_TOKEN.unlockDevice(SERIAL);
		Assert.assertFalse(unlockDevice);
	}

	@Test
	public void shouldListAllDevices() {
		List<Device> devices = STF.devices();
		Assert.assertEquals(CONNECTED_DEVICES, devices.size());
	}

	@Test
	public void shouldGetEmptyListCauseInvalidToken() {
		List<Device> devices = STF_INVALID_TOKEN.devices();
		Assert.assertEquals(0, devices.size());
	}

	@Test
	public void shouldGetDeviceInfo() {
		Optional<Device> device = STF.device(SERIAL);
		if (device.isPresent()) {
			Assert.assertEquals(SERIAL, device.get().getSerial());
		} else {
			Assert.fail();
		}
	}

	@Test
	public void shouldGetEmptyDeviceInfoCauseInvalidToken() {
		Optional<Device> device = STF_INVALID_TOKEN.device(SERIAL);
		Assert.assertFalse(device.isPresent());
	}

	@Test
	public void shouldGetAllDevicesLockedByThisUser() {
		STF.lockDevice(SERIAL);

		List<Device> userDevices = STF.userDevices();
		Assert.assertEquals(LOCKED_DEVICES, userDevices.size());

		STF.unlockDevice(SERIAL);
	}

	@Test
	public void shouldGetEmptyListForThisUserCauseInvalidToken() {
		List<Device> userDevices = STF_INVALID_TOKEN.userDevices();
		Assert.assertEquals(0, userDevices.size());
	}

	@Test
	public void shouldGetUser() {
		Optional<User> user = STF.user();
		if (user.isPresent()) {
			Assert.assertEquals(NAME, user.get().getName());
		} else {
			Assert.fail();
		}
	}

	@Test
	public void shouldGetEmptyUserCauseInvalidToken() {
		Optional<User> user = STF_INVALID_TOKEN.user();
		Assert.assertFalse(user.isPresent());
	}
}