package br.com.rsinet.labmobile.hub.selenium.grid;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class SeleniumGridIT {

	private static final String hubUrl = "http://localhost:4444";
	private static URL uri;
	private static Path hubConfig;
	private static Path firefoxNodeConfig;
	private static Path chromeNodeConfig;
	private SeleniumHub hub = SeleniumHub.getInstance();
	private SeleniumNode node;

	
	@BeforeClass
	public static void setUpClass() throws URISyntaxException, MalformedURLException {
		uri = new URL(hubUrl + "/wd/hub");

		URI hubConfigLocation = ClassLoader.getSystemResource("hubConfig.json").toURI();
		hubConfig = Paths.get(hubConfigLocation);
		URI firefoxNodeConfigLocation = ClassLoader.getSystemResource("firefoxNodeConfig.json").toURI();
		firefoxNodeConfig = Paths.get(firefoxNodeConfigLocation);
		URI chromeNodeConfigLocation = ClassLoader.getSystemResource("chromeNodeConfig.json").toURI();
		chromeNodeConfig = Paths.get(chromeNodeConfigLocation);
		
//		URI geckoDriverUri = ClassLoader.getSystemResource("drivers.geckodriver").toURI();
//		URI chromeDriverUri = ClassLoader.getSystemResource("drivers.chromedriver").toURI();
		
		System.setProperty("webdriver.gecko.driver", Paths.get("drivers/geckodriver").toString());
		System.setProperty("webdriver.chrome.driver", Paths.get("drivers/chromedriver").toString());
	}

	@After
	public void tearDown() {
		if (node.isRunning())
			node.shutdown();
		if (hub.isRunning())
			hub.shutdown();
	}

	@Test
	public void shouldRunAutomationScriptOnFirefox() {
		node =  SeleniumNode.newInstance(firefoxNodeConfig);
		
		hub.start(hubConfig);
		node.start();

		Capabilities capabilities = DesiredCapabilities.firefox();
		WebDriver driver = new RemoteWebDriver(uri, capabilities);
		driver.navigate().to("http://google.com.br");
		driver.quit();
	}
	
	@Test
	public void shouldRunAutomationScriptOnChrome() {
		node =  SeleniumNode.newInstance(chromeNodeConfig);
		
		hub.start(hubConfig);
		node.start();

		Capabilities capabilities = DesiredCapabilities.chrome();
		WebDriver driver = new RemoteWebDriver(uri, capabilities);
		driver.navigate().to("http://google.com.br");
		driver.quit();
	}
}