package br.com.rsinet.labmobile.hub;

import java.nio.file.Paths;
import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;

import br.com.rsinet.labmobile.hub.cli.Arguments;
import br.com.rsinet.labmobile.hub.selenium.grid.SeleniumHub;

public class Main {
	static {
		// Adaptadores para Log4j2.
		System.setProperty("java.util.logging.manager", "org.apache.logging.log4j.jul.LogManager");
		System.setProperty("org.seleniumhq.jetty9.util.log.class", "org.seleniumhq.jetty9.util.log.JavaUtilLog");
	}

	private static final Logger LOGGER = LogManager.getLogger();
	private static final String NAME = "RSI Automation Hub Service";
	private static final String JAR_NAME = "hub.jar";

	public static void main(String[] args) {
		args = new String[] { "-c", "src/main/resources/hubConfig.json" };

		Arguments config = Arguments.newInstance();
		JCommander cli = JCommander.newBuilder().addObject(config).programName(JAR_NAME).build();

		try {
			cli.parse(args);
		} catch (ParameterException e) {
			System.out.println(e.getLocalizedMessage());
			cli.usage();
			System.exit(1);
		}

		if (config.isHelp()) {
			cli.usage();
			System.exit(1);
		}

		LOGGER.info("*****Initializing " + NAME + "*****\n");

		SeleniumHub hub = SeleniumHub.getInstance();
		hub.start(Paths.get(config.getHubConfigFilePath()));
		// new Scanner(System.in).nextLine();
		// hub.shutdownNodes();
		// hub.shutdown();
	}
}