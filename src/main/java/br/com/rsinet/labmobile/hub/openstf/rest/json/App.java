package br.com.rsinet.labmobile.hub.openstf.rest.json;

public class App {

	private String id;
	private String name;
	private boolean selected;
	private boolean system;
	private String type;
	private String developer;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	public boolean isSystem() {
		return system;
	}
	public void setSystem(boolean system) {
		this.system = system;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDeveloper() {
		return developer;
	}
	public void setDeveloper(String developer) {
		this.developer = developer;
	}
	@Override
	public String toString() {
		return "App [id=" + id + ", name=" + name + ", selected=" + selected + ", system=" + system + ", type=" + type
				+ ", developer=" + developer + "]";
	}
	
	

}
