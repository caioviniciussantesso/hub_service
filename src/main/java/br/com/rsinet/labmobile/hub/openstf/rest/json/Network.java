package br.com.rsinet.labmobile.hub.openstf.rest.json;

public class Network {

	private boolean connected;
	private boolean failover;
	private boolean roaming;
	private String subtype;
	private String type;

	public boolean isConnected() {
		return connected;
	}

	public void setConnected(boolean connected) {
		this.connected = connected;
	}

	public boolean isFailover() {
		return failover;
	}

	public void setFailover(boolean failover) {
		this.failover = failover;
	}

	public boolean isRoaming() {
		return roaming;
	}

	public void setRoaming(boolean roaming) {
		this.roaming = roaming;
	}

	public String getSubtype() {
		return subtype;
	}

	public void setSubtype(String subtype) {
		this.subtype = subtype;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "Network [connected=" + connected + ", failover=" + failover + ", roaming=" + roaming + ", subtype="
				+ subtype + ", type=" + type + "]";
	}

}
