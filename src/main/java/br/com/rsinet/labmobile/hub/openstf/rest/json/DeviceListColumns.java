package br.com.rsinet.labmobile.hub.openstf.rest.json;

public class DeviceListColumns {
	private String name;
	private boolean selected;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	@Override
	public String toString() {
		return "DeviceListColumns [name=" + name + ", selected=" + selected + "]";
	}

}
