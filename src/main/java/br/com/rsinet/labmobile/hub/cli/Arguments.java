package br.com.rsinet.labmobile.hub.cli;

import org.openqa.grid.internal.utils.configuration.validators.FileExistsValueValidator;

import com.beust.jcommander.Parameter;

public class Arguments {

	private static final Arguments INSTANCE = new Arguments();

	@Parameter(names = { "--hubConfig",
			"-c" }, description = "<String> filename: a JSON file (following grid2 format), which defines the hub properties.", validateValueWith = FileExistsValueValidator.class, required = true)
	private String hubConfigFilePath;

	@Parameter(names = { "--help", "-h" }, description = "Displays it.", help = true)
	private boolean help;

	public static Arguments newInstance() {
		return INSTANCE;
	}

	private Arguments() {
	}

	public String getHubConfigFilePath() {
		return hubConfigFilePath;
	}

	public boolean isHelp() {
		return help;
	}

}