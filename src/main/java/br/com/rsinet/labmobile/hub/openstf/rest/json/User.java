package br.com.rsinet.labmobile.hub.openstf.rest.json;

public class User {

	private String createdAt;
	private String email;
	// forwards
	private String group;
	private String ip;
	private String lastLoggedInAt;
	private String name;
	private Settings settings;

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getLastLoggedInAt() {
		return lastLoggedInAt;
	}

	public void setLastLoggedInAt(String lastLoggedInAt) {
		this.lastLoggedInAt = lastLoggedInAt;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Settings getSettings() {
		return settings;
	}

	public void setSettings(Settings settings) {
		this.settings = settings;
	}

	@Override
	public String toString() {
		return "User [createdAt=" + createdAt + ", email=" + email + ", group=" + group + ", ip=" + ip
				+ ", lastLoggedInAt=" + lastLoggedInAt + ", name=" + name + ", settings=" + settings + "]";
	}

}
