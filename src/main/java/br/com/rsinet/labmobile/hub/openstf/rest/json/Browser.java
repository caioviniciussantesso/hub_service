package br.com.rsinet.labmobile.hub.openstf.rest.json;

import java.util.List;

public class Browser {

	private List<App> apps;
	private boolean selected;

	public List<App> getApps() {
		return apps;
	}

	public void setApps(List<App> apps) {
		this.apps = apps;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	@Override
	public String toString() {
		return "Browser [apps=" + apps + ", selected=" + selected + "]";
	}

}
