package br.com.rsinet.labmobile.hub.selenium.grid;

import java.io.IOException;
import java.net.ServerSocket;
import java.nio.file.Files;
import java.nio.file.Path;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.grid.common.RegistrationRequest;
import org.openqa.grid.internal.utils.configuration.GridNodeConfiguration;
import org.openqa.selenium.remote.server.SeleniumServer;

public class SeleniumNodeImpl implements SeleniumNode {

	private static final Logger LOGGER = LogManager.getLogger();
	private CustomSelfRegisteringRemote remote;
	private boolean running;
	private final Path nodeConfig;

	private SeleniumNodeImpl(Path nodeConfig) {
		if (nodeConfig == null || Files.notExists(nodeConfig))
			throw new IllegalArgumentException("Selenium Node's JSON file does not exist.");
		this.nodeConfig = nodeConfig;
	}

	static SeleniumNode newInstance(Path nodeConfig) {
		return new SeleniumNodeImpl(nodeConfig);
	}

	@Override
	public void start() {
		if (running)
			throw new IllegalStateException("Selenium Node already running.");

		GridNodeConfiguration configuration = GridNodeConfiguration.loadFromJSON(nodeConfig.toString());
//		configuration.servlets = Arrays.asList("org.openqa.grid.web.servlet.LifecycleServlet");
		if (configuration.host == null)
			configuration.port = getAvailableLocalPort(configuration.port);

		RegistrationRequest c = RegistrationRequest.build(configuration);
		remote = new CustomSelfRegisteringRemote(c);
		SeleniumServer server = new SeleniumServer(c.getConfiguration());

		remote.setRemoteServer(server);
		try {
			remote.startRemoteServer();
		} catch (Exception e) {
			LOGGER.error(e);
		}
		remote.startRegistrationProcess();
		running = true;
	}

	private int getAvailableLocalPort(Integer port) {
		if (port == null)
			port = 5555;

		for (; port < 65536; port++) {
			try {
				new ServerSocket(port).close();
				return port;
			} catch (IOException e) {
			}
		}
		throw new RuntimeException();
	}

	@Override
	public boolean isRunning() {
		return running;
	}

	@Override
	public void shutdown() {
		if (!running)
			throw new IllegalStateException("Selenium Node is not running.");

		running = false;
		remote.stopRemoteServer();
	}

	@Override
	public String getUrl() {
		if (!running)
			throw new IllegalStateException("Selenium Node is not running.");
		return remote.getRemoteURL().toString();
	}
}