package br.com.rsinet.labmobile.hub.selenium.grid;

import java.nio.file.Path;

/**
 * Selenium Hub.
 * <p>
 * Pode ser desligado com um GET para
 * http://localhost:4444/lifecycle-manager?action=shutdown, no entanto o
 * processo é morto.
 * 
 * @author caiosantesso
 *
 */
public interface SeleniumHub {

	public static SeleniumHub getInstance() {
		return SeleniumHubImpl.getInstance();
	}

	/**
	 * Inicia o Selenium Hub com as configurações contidas no arquivo JSON de
	 * <strong>hubConfig</strong>.
	 * 
	 * <p>
	 * Template do arquivo JSON
	 * https://github.com/SeleniumHQ/selenium/blob/master/java/server/src/org/openqa/grid/common/defaults/DefaultHub.json
	 * 
	 * @param hubConfig
	 *            JSON com as configurações do hub.
	 * 
	 * @throws IllegalStateException
	 *             caso chamado quando o Selenium Hub estiver em execução.
	 */
	void start(Path hubConfig);

	/**
	 * Desliga o hub.
	 * 
	 * @throws IllegalStateException
	 *             caso chamado quando o Selenium Hub não estiver em execução.
	 */
	void shutdown();

	/**
	 * Informa se o Selenium Hub está em execução.
	 * 
	 * @return
	 */
	boolean isRunning();

}