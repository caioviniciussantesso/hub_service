package br.com.rsinet.labmobile.hub.openstf.rest.json;

public class Owner {

	private String email;
	private String group;
	private String name;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Owner [email=" + email + ", group=" + group + ", name=" + name + "]";
	}

}
