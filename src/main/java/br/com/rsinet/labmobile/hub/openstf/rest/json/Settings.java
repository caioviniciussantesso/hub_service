package br.com.rsinet.labmobile.hub.openstf.rest.json;

import java.util.List;

public class Settings {
	private DeviceListActiveTabs deviceListActiveTabs;
	private List<DeviceListColumns> deviceListColumns;
	private String lastUsedDevice;

	public DeviceListActiveTabs getDeviceListActiveTabs() {
		return deviceListActiveTabs;
	}

	public void setDeviceListActiveTabs(DeviceListActiveTabs deviceListActiveTabs) {
		this.deviceListActiveTabs = deviceListActiveTabs;
	}

	public List<DeviceListColumns> getDeviceListColumns() {
		return deviceListColumns;
	}

	public void setDeviceListColumns(List<DeviceListColumns> deviceListColumns) {
		this.deviceListColumns = deviceListColumns;
	}

	public String getLastUsedDevice() {
		return lastUsedDevice;
	}

	public void setLastUsedDevice(String lastUsedDevice) {
		this.lastUsedDevice = lastUsedDevice;
	}

	@Override
	public String toString() {
		return "Settings [deviceListActiveTabs=" + deviceListActiveTabs + ", deviceListColumns=" + deviceListColumns
				+ ", lastUsedDevice=" + lastUsedDevice + "]";
	}

}
