package br.com.rsinet.labmobile.hub.selenium.grid;

import java.nio.file.Path;

import org.openqa.grid.web.servlet.LifecycleServlet;

public interface SeleniumNode {

	static SeleniumNode newInstance(Path configFile) {
		return SeleniumNodeImpl.newInstance(configFile);
	}

	/**
	 * Desliga o nó.
	 * <p>
	 * ATENÇÃO: A implementação de {@link LifecycleServlet} utiliza
	 * {@link System#exit(int)}, logo não é possível executar nenhuma instrução após
	 * a chamada deste método.
	 */
	void shutdown();

	boolean isRunning();

	void start();
	
	String getUrl();

}
