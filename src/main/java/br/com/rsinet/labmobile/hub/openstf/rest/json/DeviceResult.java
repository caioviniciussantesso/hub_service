package br.com.rsinet.labmobile.hub.openstf.rest.json;

public class DeviceResult {

	private boolean success;
	private Device device;

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public Device getDevice() {
		return device;
	}

	public void setDevice(Device device) {
		this.device = device;
	}

	@Override
	public String toString() {
		return "DeviceResult [success=" + success + ", device=" + device + "]";
	}

}
