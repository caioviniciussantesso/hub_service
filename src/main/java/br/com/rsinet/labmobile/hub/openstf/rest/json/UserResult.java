package br.com.rsinet.labmobile.hub.openstf.rest.json;

public class UserResult {

	private boolean success;
	private User user;
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	@Override
	public String toString() {
		return "UserResult [success=" + success + ", user=" + user + "]";
	}
	
	
}
