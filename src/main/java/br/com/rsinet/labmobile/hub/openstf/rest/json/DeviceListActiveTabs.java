package br.com.rsinet.labmobile.hub.openstf.rest.json;

public class DeviceListActiveTabs {

	private boolean details;
	private boolean icons;

	public boolean isDetails() {
		return details;
	}

	public void setDetails(boolean details) {
		this.details = details;
	}

	public boolean isIcons() {
		return icons;
	}

	public void setIcons(boolean icons) {
		this.icons = icons;
	}

	@Override
	public String toString() {
		return "DeviceListActiveTabs [details=" + details + ", icons=" + icons + "]";
	}

}
