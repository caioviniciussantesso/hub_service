package br.com.rsinet.labmobile.hub.openstf.rest;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.logging.LoggingFeature;

import br.com.rsinet.labmobile.hub.openstf.rest.json.Device;
import br.com.rsinet.labmobile.hub.openstf.rest.json.DeviceResult;
import br.com.rsinet.labmobile.hub.openstf.rest.json.DevicesResult;
import br.com.rsinet.labmobile.hub.openstf.rest.json.RestResult;
import br.com.rsinet.labmobile.hub.openstf.rest.json.User;
import br.com.rsinet.labmobile.hub.openstf.rest.json.UserResult;

/**
 * Chamadas REST para a API do OpenSTF.
 * 
 * @author caiosantesso
 *
 */
public class OpenSTF {

	private final Client client;
	private final String baseUri;
	private final String token;
	private static final Logger LOGGER = Logger.getLogger(OpenSTF.class.getName());
	private static final Jsonb JSONB = JsonbBuilder.create();
	private static final Level ERROR = Level.WARNING;
	private static final Level INFO = Level.INFO;

	private OpenSTF(Builder builder) {
		client = ClientBuilder.newClient();

		if (builder.logging) {
			Handler handler = new ConsoleHandler();
			handler.setLevel(Level.ALL);
			LOGGER.addHandler(handler);
			LOGGER.setLevel(Level.ALL);
			client.register(new LoggingFeature(LOGGER));
		}

		token = builder.token;
		baseUri = builder.uri;
	}

	public static class Builder {

		private String uri;
		private String token;
		private boolean logging;

		/**
		 * URI para acesso ao OpenSTF.
		 * 
		 * @param uri
		 * @return
		 */
		public Builder uri(String uri) {
			this.uri = uri;
			return this;
		}

		/**
		 * Token gerado na UI do OpenSTF para autenticar usuário.
		 * 
		 * @param token
		 * @return
		 */
		public Builder token(String token) {
			this.token = token;
			return this;
		}

		/**
		 * Produz log das requisições e respostas realizadas pela implementação do
		 * JAX-RS.
		 * 
		 * @return
		 */
		public Builder logging() {
			logging = true;
			return this;
		}

		public OpenSTF build() {
			Objects.requireNonNull(uri);
			if (uri.isEmpty())
				throw new IllegalArgumentException("empty URI");
			Objects.requireNonNull(token);
			if (token.isEmpty())
				throw new IllegalArgumentException("empty token");
			return new OpenSTF(this);
		}
	}

	// ----------------DEVICES

	/**
	 * Lista todos os dispositivos, independente de usuário autenticado.
	 * <p>
	 * <strong>GET /devices</strong>
	 * 
	 * @return
	 */
	public List<Device> devices() {
		WebTarget targetUri = client.target(baseUri).path("/api/v1/devices");
		Response response = targetUri.request().header("Authorization", "Bearer " + token).get();

		String entity = response.readEntity(String.class);

		if (response.getStatus() == 401) {
			RestResult result = JSONB.fromJson(entity, RestResult.class);
			LOGGER.log(ERROR, "GET request failed: " + result.getDescription() + ".");

			return Collections.emptyList();

		} else if (response.getStatus() == 200) {
			DevicesResult result = JSONB.fromJson(entity, DevicesResult.class);
			LOGGER.log(INFO, result.toString());

			return result.getDevices();
		}

		LOGGER.log(ERROR, "Unexpected status code from response.");

		return Collections.emptyList();

	}

	/**
	 * Devolve instância de {@link Device} correspondente ao serial informado.
	 * <p>
	 * <strong>GET /devices/{serial}</strong>
	 * 
	 * @param serial
	 *            do dispositivo.
	 * @return
	 */
	public Optional<Device> device(String serial) {
		WebTarget targetUri = client.target(baseUri).path("/api/v1/devices/" + serial);
		Response response = targetUri.request().header("Authorization", "Bearer " + token).get();

		String entity = response.readEntity(String.class);

		if (response.getStatus() == 401) {
			RestResult result = JSONB.fromJson(entity, RestResult.class);
			LOGGER.log(ERROR, "GET request failed: " + result.getDescription() + ".");

			return Optional.empty();

		} else if (response.getStatus() == 200) {
			DeviceResult result = JSONB.fromJson(entity, DeviceResult.class);
			LOGGER.log(INFO, result.toString());

			return Optional.of(result.getDevice());
		}

		LOGGER.log(ERROR, "Unexpected status code from response.");

		return Optional.empty();
	}

	// ----------------USER

	/**
	 * Fornece dados sobre o usuário autenticado (relativo ao token utilizado na
	 * construção dessa instância).
	 * <p>
	 * <strong>GET /user</strong>
	 * 
	 * @return {@link User} caso o <em>status</em> da resposta for 200, e
	 *         {@link Optional#empty()} caso contrário.
	 */
	public Optional<User> user() {
		WebTarget targetUri = client.target(baseUri).path("/api/v1/user");
		Response response = targetUri.request().header("Authorization", "Bearer " + token).get();
		LOGGER.log(INFO, "GET request to " + targetUri.getUri());

		String entity = response.readEntity(String.class);

		if (response.getStatus() == 401) {
			RestResult result = JSONB.fromJson(entity, RestResult.class);
			LOGGER.log(ERROR, "GET request failed: " + result.getDescription() + ".");

			return Optional.empty();

		} else if (response.getStatus() == 200) {
			UserResult result = JSONB.fromJson(entity, UserResult.class);
			LOGGER.log(INFO, result.toString());

			return Optional.of(result.getUser());
		}

		LOGGER.log(ERROR, "Unexpected status code from response.");

		return Optional.empty();
	}

	/**
	 * Fornece lista dos dispositivos travados pelo usuário autenticado (relativo ao
	 * token utilizado na construção dessa instância).
	 * <p>
	 * <strong>GET /user</strong>
	 * 
	 * @return Lista de {@link Device} caso o <em>status</em> da resposta for 200, e
	 *         uma lista vazia caso contrário.
	 */
	public List<Device> userDevices() {
		WebTarget targetUri = client.target(baseUri).path("/api/v1/user/devices");
		Response response = targetUri.request().header("Authorization", "Bearer " + token).get();
		LOGGER.log(INFO, "GET request to " + targetUri.getUri());

		String entity = response.readEntity(String.class);

		if (response.getStatus() == 401) {
			RestResult result = JSONB.fromJson(entity, RestResult.class);
			LOGGER.log(ERROR, "GET request failed: " + result.getDescription() + ".");

			return Collections.emptyList();

		} else if (response.getStatus() == 200) {
			DevicesResult result = JSONB.fromJson(entity, DevicesResult.class);
			LOGGER.log(INFO, result.toString());

			return result.getDevices();
		}

		LOGGER.log(ERROR, "Unexpected status code from response.");

		return Collections.emptyList();
	}

	/**
	 * Bloqueia o dispositivo, atrelado ao usuário atual, com o
	 * <strong>serial</strong> informado.
	 * <p>
	 * <strong>POST /user/devices</strong>
	 * 
	 * @param serial
	 * @return
	 */
	public boolean lockDevice(String serial) {
		WebTarget targetUri = client.target(baseUri).path("/api/v1/user/devices");

		Response response = targetUri.request().header("Authorization", "Bearer " + token)
				.accept(MediaType.APPLICATION_JSON_TYPE).post(Entity.json("{\"serial\":\"" + serial + "\"}"));

		String entity = response.readEntity(String.class);
		RestResult result = JSONB.fromJson(entity, RestResult.class);

		if (response.getStatus() == 401) {
			LOGGER.log(ERROR, "POST request failed: " + result.getDescription() + ".");

			return false;

		} else if (response.getStatus() == 403) {
			LOGGER.log(ERROR, "Locking failed: " + result.getDescription() + ".");

			return false;

		} else if (response.getStatus() == 200) {
			LOGGER.log(INFO, "Device was successful locked.");

			return true;
		}

		LOGGER.log(ERROR, "Unexpected status code from response.");

		return false;
	}

	/**
	 * Desbloqueia o dispositivo, atrelado ao usuário atual, com o
	 * <strong>serial</strong> informado.
	 * <p>
	 * <strong>DELETE /user/devices/{serial}</strong>
	 * 
	 * @param serial
	 * @return
	 */
	public boolean unlockDevice(String serial) {
		WebTarget targetUri = client.target(baseUri).path("/api/v1/user/devices/" + serial);

		Response response = targetUri.request().header("Authorization", "Bearer " + token).delete();

		String entity = response.readEntity(String.class);
		RestResult result = JSONB.fromJson(entity, RestResult.class);

		if (response.getStatus() == 401) {
			LOGGER.log(ERROR, "DELETE request failed: " + result.getDescription() + ".");

			return false;

		} else if (response.getStatus() == 403) {
			LOGGER.log(ERROR, "Unlocking failed: " + result.getDescription() + ".");

			return false;

		} else if (response.getStatus() == 200) {
			LOGGER.log(INFO, "Device was successful unlocked.");

			return true;
		}

		LOGGER.log(ERROR, "Unexpected status code from response.");

		return false;
	}

}