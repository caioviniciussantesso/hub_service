package br.com.rsinet.labmobile.hub.selenium.grid;

import java.nio.file.Files;
import java.nio.file.Path;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.grid.internal.utils.configuration.GridHubConfiguration;
import org.openqa.grid.web.Hub;

class SeleniumHubImpl implements SeleniumHub {

	private static final Logger logger = LogManager.getLogger();
	private static final SeleniumHub INSTANCE = new SeleniumHubImpl();
	private static Hub hub;
	private static boolean running;

	private SeleniumHubImpl() {
	}

	public static SeleniumHub getInstance() {
		return INSTANCE;
	}

	@Override
	public void start(Path config) {
		if (config == null || Files.notExists(config))
			throw new IllegalArgumentException("Selenium Hub's JSON file does not exist.");

		if (running)
			throw new IllegalStateException("Selenium Hub already running.");

		GridHubConfiguration conf = GridHubConfiguration.loadFromJSON(config.toAbsolutePath().toString());
		hub = new Hub(conf);
		try {
			logger.info("Starting up Selenium Hub.");
			hub.start();
			running = true;
		} catch (Exception e) {
			logger.error(e); // Generic Error from Selenium. Can't handle.
		}
		logger.info("Hub configuration: {}", hub.getConfiguration());

	}

	@Override
	public void shutdown() {
		if (!running)
			throw new IllegalStateException("Selenium Server's not running.");

		try {
			hub.getRegistry().stop();
			logger.info("Shutting down Selenium Hub.");
			hub.stop();
			running = false;
		} catch (Exception e) {
			logger.error(e);
		}
	}

	@Override
	public boolean isRunning() {
		return running;
	}
}