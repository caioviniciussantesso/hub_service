package br.com.rsinet.labmobile.hub.openstf.rest.json;

import java.util.List;

public class DevicesResult {

	private boolean success;
	private List<Device> devices;
	
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public List<Device> getDevices() {
		return devices;
	}
	public void setDevices(List<Device> devices) {
		this.devices = devices;
	}

	@Override
	public String toString() {
		return "DevicesResult [success=" + success + ", devices=" + devices + "]";
	}
	
	
}
