package br.com.rsinet.labmobile.hub.openstf.rest.json;

public class Provider {

	private String channel;
	private String name;

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Provider [channel=" + channel + ", name=" + name + "]";
	}

}
