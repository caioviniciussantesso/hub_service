package br.com.rsinet.labmobile.hub.openstf.rest.json;

public class Display {

	private double density;
	private int fps;
	private int height;
	private int id;
	private int rotation;
	private boolean secure;
	private double size;
	private String url;
	private int width;
	private int xdpi;
	private int ydpi;

	public double getDensity() {
		return density;
	}
	public void setDensity(double density) {
		this.density = density;
	}
	public int getFps() {
		return fps;
	}
	public void setFps(int fps) {
		this.fps = fps;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getRotation() {
		return rotation;
	}
	public void setRotation(int rotation) {
		this.rotation = rotation;
	}
	public boolean isSecure() {
		return secure;
	}
	public void setSecure(boolean secure) {
		this.secure = secure;
	}
	public double getSize() {
		return size;
	}
	public void setSize(double size) {
		this.size = size;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getXdpi() {
		return xdpi;
	}
	public void setXdpi(int xdpi) {
		this.xdpi = xdpi;
	}
	public int getYdpi() {
		return ydpi;
	}
	public void setYdpi(int ydpi) {
		this.ydpi = ydpi;
	}
	
	@Override
	public String toString() {
		return "Display [density=" + density + ", fps=" + fps + ", height=" + height + ", id=" + id + ", rotation="
				+ rotation + ", secure=" + secure + ", size=" + size + ", url=" + url + ", width=" + width + ", xdpi="
				+ xdpi + ", ydpi=" + ydpi + "]";
	}
	
	
}
