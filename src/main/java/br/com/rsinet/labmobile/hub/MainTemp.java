package br.com.rsinet.labmobile.hub;

import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

import br.com.rsinet.labmobile.hub.selenium.grid.SeleniumNode;

public class MainTemp {

	static {
		// Adaptadores para Log4j2.
		System.setProperty("java.util.logging.manager", "org.apache.logging.log4j.jul.LogManager");
		System.setProperty("org.seleniumhq.jetty9.util.log.class", "org.seleniumhq.jetty9.util.log.JavaUtilLog");
	}

	public static void main(String[] args) throws URISyntaxException {
		Path nodeConfigFile = Paths.get("src/test/resources/firefoxNodeConfig.json");
		SeleniumNode node = SeleniumNode.newInstance(nodeConfigFile);
		node.start();
		node.shutdown();
	}
}
