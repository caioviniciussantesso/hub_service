package br.com.rsinet.labmobile.hub.openstf.rest.json;

import javax.json.bind.annotation.JsonbCreator;
import javax.json.bind.annotation.JsonbProperty;

public class RestResult {
	private boolean success;
	private String description;

	@JsonbCreator
	public RestResult(
			@JsonbProperty("success") boolean success,
			@JsonbProperty("description") String description) {
		
		this.success = success;
		this.description = description;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getDescription() {
		return description;
	}

	@Override
	public String toString() {
		return "RestResult [success=" + success + ", description=" + description + "]";
	}
}