# RSI Hub (Alterar)
Serviço sempre ativo que:
* Via REST, bloqueia e desbloqueia os celulares por meio do OpenSTF.
* Via Selenium Grid, executa os testes nos nós Appium.
* 

## Chamadas REST ao OpenSTF

Nota: Substitua os dados em negrito nas chamadas abaixo!

###Bloquear celular com serial
curl -X POST --header "Content-Type: application/json" --data '{"serial":"__0422443110__"}' -H "Authorization: Bearer __82dcfe0f289e42d385388cb87473ea64561dc96aa86c4258aaa833f413933cc6__" http://localhost:7100/api/v1/user/devices

###Desbloquear celular com serial
curl -X DELETE -H "Authorization: Bearer __82dcfe0f289e42d385388cb87473ea64561dc96aa86c4258aaa833f413933cc6__" http://localhost:7100/api/v1/user/devices/__0422443110__

###Mais informações
https://github.com/openstf/stf/blob/master/doc/API.md

## Selenium Grid

### Mozilla Firefox
__geckodriver__ versão 0.18.0, para Linux 64 bits.
https://github.com/mozilla/geckodriver/releases/tag/v0.18.0

### Google Chrome
__chrome_driver__, versão 2.30, para Linux 64 bits.
https://sites.google.com/a/chromium.org/chromedriver/downloads

### selenium-server-standalone
Está em na pasta src/main/resources junto aos drivers pois não há esse JAR no Maven Central.

### Grid Console
http://localhost:4444/grid/console

### Endpoints
* http://localhost:4444/grid/console
* http://localhost:4444/wd/hub/
* http://localhost:4444/grid/api/proxy/
* http://localhost:4444/grid/api/hub/
* http://localhost:4444/grid/api/testsession/

### Exemplo de script para iniciar selenium-server-standalone

<pre>
#!/bin/sh
# Selenium Node para Firefox Linux 64 bits.
java -jar selenium-server-standalone-3.4.0.jar -role node \
-Dwebdriver.gecko.driver=geckodriver \
-hub http://localhost:4444/grid/register \
-browser browserName=firefox,platform=LINUX -port 5557
-nodeConfig firefoxNodeConfig.json
</pre>

